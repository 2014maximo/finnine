export class PersonasModel{

    public id: string;
    public nombre: string;
    public apellido: string;
    public email: string;
    public ip: string;
    
}