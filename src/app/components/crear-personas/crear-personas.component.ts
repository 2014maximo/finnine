import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Message } from 'primeng/components/common/message';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonasModel } from '../../models/personas.model';
import { PersonasService } from '../../services/personas.service';

@Component({
  selector: 'app-crear-personas',
  templateUrl: './crear-personas.component.html',
  styleUrls: []
})
export class CrearPersonasComponent implements OnInit {

  form: FormGroup;
  msgs: Message[] = [];
  personas: PersonasModel;
  idPersonas: string;
  estado: boolean;
  verActualizar = false;

  constructor(private fb: FormBuilder,
              private personasService: PersonasService,
              private router: ActivatedRoute,
              private routerNav: Router) {
    this.form = this.fb.group({
      id: [null],
      nombre: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/)// solo letras
        ]],
      apellido: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/)// solo letras
        ]],
      email: ['',
        [
          Validators.required,
          Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/), // correo debe tner @ y .com
        ]],
      ip: ['',
        [
          Validators.required,
          Validators.pattern(/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/)
        ]]
    });
  }

  ngOnInit() {
    this.idPersonas = this.router.snapshot.paramMap.get('idpersonas');

    if (this.idPersonas) {
      this.personasService.consultar(this.idPersonas).subscribe(data => {
        this.personas = data;

        this.form.patchValue(this.personas);

      });
    }
  }

  guardar() {
    let mensaje = '';

    if (this.form.invalid) {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Debes completar la informacion del formulario.' });

    } else {

      if (!this.personas) {
        this.personas = new PersonasModel();
      }

      this.personas.nombre = this.form.get('nombre').value;
      this.personas.apellido = this.form.get('apellido').value;
      this.personas.email = this.form.get('email').value;
      this.personas.ip = this.form.get('ip').value;

      if (this.idPersonas) {

        this.personasService.actualizar(this.personas).subscribe(data => {

          this.form.reset();
          mensaje = 'Registro actualizado con éxito';
          this.routerNav.navigate(['/listar', mensaje]);

        });

      } else {
        this.personasService.guardar(this.personas).subscribe(data => {

          this.form.reset();
          mensaje = 'Registro almacenado con exito';
          this.routerNav.navigate(['/listar', mensaje]);
        });
      }
    }
  }

}