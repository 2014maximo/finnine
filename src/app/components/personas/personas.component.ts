import { Component, OnInit } from '@angular/core';
import { Message, ConfirmationService } from 'primeng/api';
import { ActivatedRoute, Routes } from '@angular/router';
import { PersonasModel } from '../../models/personas.model';
import { PersonasService } from '../../services/personas.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: []
})
export class PersonasComponent implements OnInit {

  personas: PersonasModel[];
  msgs: Message[] = [];

  constructor(private personasService: PersonasService,
              private confirmationService: ConfirmationService,
              private router: ActivatedRoute) {

  }

  ngOnInit() {
    this.consultarElementos();

    const mensaje = this.router.snapshot.paramMap.get('mensaje');
    this.msgs = [];
    if (mensaje) {
      this.msgs.push({
        severity: 'success',
        summary: 'Informacion',
        detail: mensaje
      });
    }
  }

  consultarElementos() {
    this.personasService.consultarPersonas().subscribe(datos => {
      this.personas = datos;
    });
  }

  confirmarEliminar(idEliminar: string, idnombre: string) {
    console.log(idEliminar);
    this.confirmationService.confirm({
      message: `Esta seguro de eliminar a ${idnombre}?`,
      accept: () => {

        this.personasService.eliminar(idEliminar).subscribe(data => {
          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: 'Informacion',
            detail: 'Registro eliminado con exito.'
          });

          this.consultarElementos();

        });
      }
    });
  }

}