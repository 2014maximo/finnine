import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersonasModel } from '../models/personas.model';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private url = 'http://localhost:3000/personas/';

  constructor( private http: HttpClient) { }

  consultarPersonas(): Observable<PersonasModel[]>{
    return this.http.get<PersonasModel[]>(this.url);
  }

  consultar(idPersonas: string): Observable<PersonasModel>{
    return this.http.get<PersonasModel>(this.url + idPersonas);
  }

  guardar(datosPersonas: PersonasModel): Observable<PersonasModel>{
    return this.http.post<PersonasModel>(this.url, datosPersonas);
  }

  actualizar(datosPersonas: PersonasModel): Observable<PersonasModel>{
    return this.http.put<PersonasModel>(this.url + datosPersonas.id, datosPersonas);
  }

  eliminar(idEliminar: string): Observable<PersonasModel>{
    return this.http.delete<PersonasModel>(`${this.url}${idEliminar}`);
  }


}
