import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrearPersonasComponent } from './components/crear-personas/crear-personas.component';
import { PersonasComponent } from './components/personas/personas.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

import { HttpClientModule } from '@angular/common/http';

//NG PRIME
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GalleriaModule } from 'primeng/galleria';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

//RUTAS
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';

//FORMULARIOS
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CanActivateViaAuthGuard } from './components/shared/guard/autho-guard';
import { TabsComponent } from './components/tabs/tabs.component';

@NgModule({
  declarations: [
    AppComponent,
    CrearPersonasComponent,
    PersonasComponent,
    NavbarComponent,
    TabsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    GalleriaModule,
    MessagesModule,
    MessageModule,
    ConfirmDialogModule,
    RouterModule.forRoot( ROUTES)
  ],
  providers: [
    ConfirmationService,
    CanActivateViaAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
