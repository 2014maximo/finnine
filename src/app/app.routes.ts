import { Routes } from '@angular/router';
import { PersonasComponent } from './components/personas/personas.component';
import { CrearPersonasComponent } from './components/crear-personas/crear-personas.component';
import { CanActivateViaAuthGuard } from './components/shared/guard/autho-guard';

export const ROUTES: Routes = [

    {  path: 'crear', component: CrearPersonasComponent, canActivate: [CanActivateViaAuthGuard] },
    {  path: 'crear/:idpersonas', component: CrearPersonasComponent, canActivate: [CanActivateViaAuthGuard] },
    {  path: 'listar', component: PersonasComponent, canActivate: [CanActivateViaAuthGuard] },
    {  path: 'listar/:mensaje', component: PersonasComponent, canActivate: [CanActivateViaAuthGuard] },

];
